module gitlab.com/juhani/go-semrel-gitlab

go 1.16

require (
	github.com/bisegni/go-semrel v0.0.0-20210507094722-8daa82e6fe9f
	github.com/blang/semver v3.5.1+incompatible
	github.com/golang/protobuf v1.3.0 // indirect
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/pkg/errors v0.8.1
	github.com/urfave/cli v1.20.0
	github.com/xanzy/go-gitlab v0.16.0
	golang.org/x/oauth2 v0.0.0-20190226205417-e64efc72b421 // indirect
	golang.org/x/sys v0.0.0-20190813064441-fde4db37ae7a // indirect
	golang.org/x/tools v0.0.0-20190813142322-97f12d73768f // indirect
	gopkg.in/src-d/go-git.v4 v4.13.1
)
